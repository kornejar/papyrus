#ifndef __LINKED_LIST_H__
#define __LINKED_LIST_H__

#include <stdio.h>
#include <stdlib.h>

#define true 1
#define false 0
#define OK 0
#define NOK -1

//-----Structury--------------------------------------------------------------------

// linked list
typedef struct {
	void *prev;
	void *next;
	char znak;
} entry_t;

// hlavicka linkoveho seznamu
/*
* Je identicka pro vsechny zapisy. Obsahuje adresu prvniho a posledniho prvku a take 
* velikost (delku) listu. 
*/
typedef struct {
	entry_t *head;
	entry_t *tail;
	int size;
} list_t;

//-----Hlavicky---------------------------------------------------------------------

/*
 * Push the entry value to the queue.
 * return: true on success and false otherwise.
 */
_Bool Push(list_t *header, char znak);

/*
 * Return (pop) last item from list.
 */
int Pop(list_t *header);

/*
 * Insert the given entry to index.
 *
 * return: true on success; false otherwise
 */
_Bool Insert(list_t *header, char znak, int idx);

/*
 * Erase entry on that index
 */
_Bool EraseEntry(list_t *header, int idx);

/*
 * For idx >= 0 and idx < size(queue), it returns the particular item
 * stored at the idx-th position of the queue. The head of the queue
 * is the entry at idx = 0.
 *
 * return: the particular value of the entry at the idx-th position or
 * value less than 0 to indicate the requested position is not presented
 * in the queue 
 */
char getEntry(list_t *header, int idx);

/*
 * return: the number of stored items in the queue
 */
int size(list_t *header);

/*
 * Remove all entries in the linked list
 */
void clear(list_t *header);
 
#endif
