#ifndef __TOOLS_H__
#define __TOOLS_H__
//-----Knihovny---------------------------------------------------------------------
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h> 
#include <unistd.h>

#define true 1
#define false 0
#define OK 0
#define NOK -1

#define SIZE 100

//-----Hlavicky---------------------------------------------------------------------

/*
 * Identicka funkce k getline()
 */
int My_getline (char **lineptr, size_t *n, FILE *stream);

/*
 * Set terminal to non-block and back to blok mode.
 */
void Call_termios(int reset);

/*
 * Merge to string a save output to the third.
 */
int Str_merge (char ** str_out, char * str_in1, char * str_in2);

/*
 * Copy and aloc
 */
int str_copy (char ** str_out, char * str_in);

/*
 * Evaluate text yes/no input and return true/false.
 */
int Yes_no (char *string);

int Get_terminal_size (int *row, int *col);
int Get_cursor_position (int *row, int *col);

#endif