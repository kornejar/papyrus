// By Pytel

#include "tools.h"

#define DEBUG 0

//-----Podprogramy------------------------------------------------------------------

int My_getline (char **lineptr, size_t *n, FILE *stream){
	int increment = 10, nactenych = 0;
	size_t alokovano = *n;
	char *line = *lineptr, znak = NOK;
	
	// Alokace nove pameti
	if (*lineptr == NULL || *n == 0){
		if ( (line=(char*)malloc(increment*sizeof(char))) == NULL){
			fprintf(stderr, "Error: Nepodarilo se alokovat misto!\n");
			return NOK;
		}
	}
	
	znak = getc(stream);
	while ( znak != '\n' ){
		//printf("Znak: %c\n", znak);
		nactenych++;
		if (nactenych >= alokovano){
	// musim doalokovat
			alokovano = alokovano + increment;
			line = (char*) realloc (line, alokovano*sizeof(char));
			if ( line == NULL){
				fprintf(stderr, "Error: Nepodarilo se alokovat misto!\n");
				return NOK;
			}
		}
		
	// mohu jej pridat
		line[nactenych-1] = znak;
		
		znak = getc(stream);
		if (znak == EOF){
			free(line);
			fprintf(stderr, "Error: prazdny retezec!\n");
			return NOK;
		}
	}
	
	// konecna realokace
	line = (char*) realloc (line, (nactenych+1)*sizeof(char));
	if ( line == NULL){
		fprintf(stderr, "Error: Nepodarilo se alokovat misto!\n");
		return NOK;
	}
	line[nactenych] = '\0';
	*lineptr = line;
	return nactenych;
}

void Call_termios(int reset) {
	static struct termios tio, tioOld;
	tcgetattr(STDIN_FILENO, &tio);
	if (reset) {
		tcsetattr(STDIN_FILENO, TCSANOW, &tioOld);
	} else {
		tioOld = tio; //backup 
		cfmakeraw(&tio);
		tio.c_oflag |= OPOST;
		tcsetattr(STDIN_FILENO, TCSANOW, &tio);
	}
}

int Str_merge (char ** str_out, char * str_in1, char * str_in2){
	int ret = OK;
	char *str_ret = NULL;
	if (DEBUG) {
		printf("str1 len: %lu\n", strlen(str_in1) );
		printf("str2 len: %lu\n", strlen(str_in2) );
	}
	str_ret = (char*) malloc ((strlen(str_in1)*sizeof(char)) + sizeof(char)*(strlen(str_in2)) +1);
	if ( str_out == NULL){
		fprintf(stderr, "Error: Nepodarilo se alokovat misto!\n");
		ret = NOK;
	}
	for (int index1 = 0; index1 < strlen(str_in1); index1++){
		str_ret[index1] = str_in1[index1];
	}
	for (int index2 = 0; index2 < strlen(str_in2); index2++){
		str_ret[strlen(str_in1)+index2] = str_in2[index2];
	}
	str_ret[strlen(str_in1)+strlen(str_in2)] = '\0';
	*str_out = str_ret;
	return ret;
}

int Yes_no (char *string) {
	if ( strlen(string) == 1 ){
		if (DEBUG) {
			printf("Zadano: %s:y\n", string);
			getchar();
		}
		if ( string[0] == 'y' || string[0] == 'Y' ) {
			return true;
		} else {
			return false;
		}
	} else {
		//printf("strlen: %lu\n", strlen(string) );
		if ( (strcmp(string, "yes") == 0) || (strcmp(string, "Yes") == 0) ){
			if (DEBUG) {
				printf("Zadano: %s:yes", string);
			}
			return true;
		} else {
			return false;
		}
	}
}

int str_copy (char ** str_out, char * str_in) {
	int len = (int)strlen(str_in)+1;
	char *str = (char*) malloc (len * sizeof(char) );
	for (int i = 0; i < len; i++) {
		str[i] = str_in[i];
	}
	str[len] = '\0';
	
	*str_out = str;
	return true;
}

int Get_terminal_size (int *row, int *col) {
	int ret = OK, n;
	printf("\033[?25l");	// hide cursor
	// druhy pokus
	printf("\n");
	int position = -1, new_position = 0;
	while ( position < new_position ) {
		printf("█");
		position = new_position;
		Get_cursor_position ( &n, &new_position);
	}
	*col = position;
	position = -1;
	new_position = 0;
	printf("\n");
	while ( position < new_position ) {
		//printf("█\n");
		printf("\n");
		position = new_position;
		Get_cursor_position (&new_position, &n);
	}
	*row = position;
	if (0 && DEBUG) {
		printf("█	Size: %dx%d\n", *row, *col);
		sleep(8);
	}
	printf("\033[?25h");	// show cursor
	return ret;
}

int Get_cursor_position (int *row, int *col) {
	int ret = OK, valid = -1;
	if (write(STDOUT_FILENO, "\033[6n", 4) != 4) return -1;
	Call_termios(0);
	valid = scanf("\033[%i;%iR", row, col);
	Call_termios(1);
	if (DEBUG) {
		if (valid == 2) {
			printf("Uspesne nacteno pozici kursoru!\n");
		}
		printf("Row: %d	col: %d\n", *row, *col);
		sleep(3);
	}
	return ret;
}

