#ifndef __CURSOR_H__
#define __CURSOR_H__

#include <stdio.h>
#include <stdlib.h>

#include "lib.h"

#define true 1
#define false 0
#define OK 0
#define NOK -1

//-----Hlavicky---------------------------------------------------------------------

/*
 * Print whole list.
 */
void Print_all(file_t * file, int *cursor);

/*
 * Moves the cursor up or down and return new index.
 * Arguments: true=up/false=down, index of cursor.
 * Return: new index of cursor.
 */
int Move_up_or_down (file_t *file, int up_or_down, int idx);
 
#endif
