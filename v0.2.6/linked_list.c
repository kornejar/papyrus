//By Pytel
/*
* Spojovy seznam. Zasobnik.
*/

#include "cursor.h"
#include "linked_list.h"

#define DEBUG 0


//-----Podprogramy------------------------------------------------------------------
void Print_entry (entry_t *list) {
	printf("Predchozi: %p\n", list->prev);
	printf("list: %p\n", list);
	printf("Nasledujici: %p\n", list->next);
	printf("Znak: %d\n", list->znak);
}

void Print_list (list_t *header) {
	printf("Hlava: %p\n", header->head);
	printf("Ocas: %p\n", header->tail);
	printf("Delka: %d\n", header->size);
}

int Alloc_list (entry_t **list) {
	entry_t *list_ret;
	if ( (list_ret =(entry_t*)malloc(sizeof(entry_t))) == NULL){
		fprintf(stderr, "Error: Nepodarilo se alokovat misto!\n");
		return NOK;
	}
	list_ret->prev = NULL;
	list_ret->next = NULL;
	list_ret->znak = '\0';
	
	*list = list_ret;
	if (DEBUG){
		printf("\n ---Alokovano!--- \n");
		printf("list: %p\n", *list);
	}
	return OK;
}

_Bool Push(list_t *header, char znak){
	_Bool ret = true;
	int valid;
	entry_t *list = NULL;
	
	// vytvoreni noveho zaznamu
	valid = Alloc_list (&list);
	if (valid == NOK) {
		return false;
	}
	list->prev = header->tail;
	list->next = NULL;
	list->znak = znak;
	
	// aktualizace predchoziho clanku
	entry_t *prev_list = header->tail;
	if (header->size != 0){		// neni prvni zaznam
		prev_list->next = list;
	} else {
		header->head = list;
	}
	
	// aktualzace hlavicky
	header->tail = list;
	header->size++;
	if (DEBUG){
		printf("\n ---Zaznamenano!--- \n");
		Print_entry (list);
	}
	return ret;
}

int Pop(list_t *header){
	int ret;
	// kotrola nenulove delky listu
	if (header->size == 0){
		return NOK;
	}
	// odebrani zapisu z hlavicky
	entry_t *list = header->tail;
	entry_t *prev_list = list->prev;
	ret = list->znak;
	
	// aktualizace hlavicky a zaznamu
	if (header->size == 1) {			// posledni zaznam
		header->head = NULL;
		header->tail = NULL;
		header->size = 0;
	} else {
		header->tail = prev_list;
		header->size--;
		prev_list->next = NULL;
	}
	
	// uvolneni pameti
	free(list);
	return ret;
}

_Bool Insert(list_t *header, char znak, int idx){
	_Bool ret = true;
	entry_t *this_list = header->head;
	
	// nulova delka
	if (header->size == 0){					// prazdny list
		Push(header, znak);
		return ret;
	}
	
	// novy (zaznam) prvek listu
	entry_t *list;
	Alloc_list (&list);
	list->znak = znak;
	
	// hledani pozice
	for (int i = 0; i < idx; i++) {
		if (this_list->next == NULL) {
			return false;
		}
		this_list = this_list->next;
	}
	// zapis na pozici
	if (header->head == list) {			// je to hlavicka
		list->next = header->head;
		header->head = list;
		list->prev = NULL;
	} else if (header->tail == list) {	// je to konec
		list->prev = header->tail;
		header->tail = list;
		list->next = NULL;
	} else {							// je to telo
		list->prev = this_list;
		list->next = this_list->next;
		entry_t *next_list = this_list->next;
		this_list->next = list;
		next_list->prev = list;
	}
	header->size++;
	
	return ret;
}

int Erase_entry (list_t *header, entry_t *list){
	if (header->size == 1) {					// posledni zaznam
		header->head = NULL;
		header->tail = NULL;
		header->size = 0;
	} else {
		entry_t *prev_list = NULL;
		entry_t *next_list = NULL;
		if (header->head == list) {			// je to hlavicka
			next_list = list->next;
			header->head = next_list;
			next_list->prev = NULL;
		} else if (header->tail == list) {	// je to konec
			prev_list = list->prev;
			header->tail = prev_list;
			prev_list->next = NULL;
		} else {							// je to telo
			prev_list = list->prev;
			next_list = list->next;
			prev_list->next = next_list;
			next_list->prev = prev_list;
		}
		header->size--;
	}
	free(list);
	return OK;
}

_Bool EraseEntry(list_t *header, int idx){
	_Bool ret = false;
	entry_t *list = header->head;
	if ( idx < 0 ) {						// test validity
		return ret;
	}
	if ( list == NULL ) {					//▬ prazdny seznam
		return ret;
	}
	for (int i = 0; i < idx; i++) {
		list = list->next;
		if (list->next == NULL) {
			return ret;
		}
	}
	ret = Erase_entry(header, list);
	return ret;
}

char getEntry(list_t *header, int idx){
	int ret = false;
	entry_t *list = header->head;
	for (int i = 0; i < idx; i++) {
		list = list->next;
		if (list->next == NULL) {
			ret = list->znak;
			return ret;
		}
	}
	if (DEBUG){
		printf("\n ---Zaznam idex: %d--- \n", idx);
		Print_entry (list);
	}
	ret = list->znak;
	return ret;
}

int size(list_t *header){
	return header->size;
}

void clear(list_t *header){
	entry_t *list = header->head;
	entry_t *next_list = NULL;
	while ( list != NULL ) {
		next_list = list->next;
		Erase_entry (header, list);
		list = next_list;						// aktualizace
	}
}


