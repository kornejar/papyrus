#ifndef __LIB_H__
#define __LIB_H__
//-----Knihovny---------------------------------------------------------------------
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h> 
#include <unistd.h>

#define true 1
#define false 0
#define OK 0
#define NOK -1

#define SIZE 100

//-----Structury--------------------------------------------------------------------

#include "linked_list.h"

typedef struct {
	list_t list;
	char *name;
} file_t;

//-----Hlavicky---------------------------------------------------------------------

// inicializace datove struktury
void file_init (file_t * file);

void file_change_name (file_t * file, char * new_name);

// free(file_t);
void file_kill (file_t * file);

/*
 * Aktivuje prostredi editoru.
 * Hlavni funkce z teto knihovny, ktera nasledně vola ostatni. 
 * Argument: file_t, s nazvem otevreneho souboru.
 */
int Editor (file_t * file);

/*
 * Otevre soubor <file->name> a nahraje jeho obsah do link-listu.
 * Nasledne zavola Editor(name);
 */
int Open_file (file_t * file);

/*
 * Vypise log programu.
 */
void Print_about (void);

/*
 * Vytiskne uvodni menu porgamu.
 */
void Print_menu (int row, int col);

/*
 * Maze obrazovku a vykresluje menu editoru
 */
void Print_editor_menu (char *name);

/*
 * Dotaze se na nazev souboru a nasledne ulozi data z link listu. 
 */
int Save (file_t * file);

int Get_terminal_size (int *row, int *col);
int Get_cursor_position (int *row, int *col);

#endif