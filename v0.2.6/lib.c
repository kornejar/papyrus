// By Pytel

#include "lib.h"
#include "tools.h"
#include "cursor.h"
#include "linked_list.h"

#define DEBUG 0

//-----Podprogramy------------------------------------------------------------------

void file_init (file_t * file) {
	file->name = NULL;
	file->list.head = NULL;
	file->list.tail = NULL;
	file->list.size = 0;
}

void file_change_name (file_t * file, char * new_name) {
	free(file->name);
	str_copy (&file->name, new_name);
}

void file_kill (file_t * file) {
	free(file->name);
	file->name = NULL;
	clear(&file->list);
}

int Editor (file_t * file) {
	int cursor = 0;
	int edit = true, valid = false;
	int number = -1;
	int row, col;
	size_t n = 0;
	
	char c;
	char *response = NULL;
	if ( file->name == NULL ) {
		file_change_name(file, "none");
		Print_editor_menu(file->name);
	} else {
		Print_editor_menu(file->name);			// nove vykresleni
		cursor = size(&file->list);
		Print_all(file, &cursor);
	}
	
	Call_termios(0);
	
	// zadavani textu
	while ( edit != false ) {	// ESC = 27
		c = getchar();
		if (DEBUG) {
			printf("\nCursor: %d	Size: %d	znak: %c\n", cursor, size(&file->list), c);
		}
		switch (c){
			case 27:	// ESC sequence
				if (DEBUG) {
					printf("\nESC seguence!");
				}
				char instruction;
				number = scanf("[%c", &instruction);
				if (1) {
					printf("\n Instruction: %c", instruction);
				}
				if ( number == EOF || number == 0 ) {
					if (1) {
						printf("\n End of input!");
					}
					edit = false;
				} else if (instruction == 'C') {		//→
					if (DEBUG) {
						printf("\n Cursor move right!");
						getchar();
					}
					if ( cursor < size(&file->list) ) {
						if ( getEntry(&file->list, cursor) < 0) { // diakritika
							if ( cursor < size(&file->list)+1 ) {
								cursor++;
								cursor++;
							}
						} else {
							cursor++;
						}
					}
				} else if (instruction == 'D') {		//←
					if (DEBUG) {
						printf("\n Cursor move left!");
						getchar();
					}
					if ( cursor > 0 ) {
						cursor--;
						if ( getEntry(&file->list, cursor) < 0 && cursor > 0) { // diakritika
							cursor--;
						}
					}
				} else if (instruction == 'A') {		//↑
					if (DEBUG) {
						printf("\n Cursor move up!");
						getchar();
					}
					cursor = Move_up_or_down (file, true, cursor);
				} else if (instruction == 'B') {		//↓
					if (DEBUG) {
						printf("\n Cursor move down!");
						getchar();
					}
					cursor = Move_up_or_down (file, false, cursor);
				} else {
					edit = false;
				}
				break;
			case 17:	// ctrl + q
				edit = false;
				break;
			case 18:	// ctrl + r
				Call_termios(1);
				Get_terminal_size (&row, &col);
				Call_termios(0);
				break;
			case 19:	// ctrl + s
				printf("\n");
				Save(file);
				Call_termios(0);
				break;
			case 127:	// DEL
				if ( cursor != 0 ) {
					cursor--;
					if ( cursor == size(&file->list)-1 ) { 
						Pop(&file->list);
					} else {
						EraseEntry(&file->list, cursor);
					}
				}
				break;
			case 13:	// Enter
				if (DEBUG) {
					printf("\nEnter!");
				}
				c = 10;
			default:
				if ( cursor == size(&file->list) ) {		// add
					Push(&file->list, c);
				} else {						// insert
					valid = Insert(&file->list, c, cursor-1);
					if (DEBUG) {
						printf("\nValid: %s	Index: %d	Size: %d", valid ? "true": "false", cursor, size(&file->list) );
						getchar();
					}
				}
				cursor++;
				break;
		}
		// vypis do terminalu
		if (DEBUG) {
			printf("Zapsano: %c	size: %d\n", getEntry(&file->list, cursor-1), size(&file->list));
			getchar();
		}
		
		Print_editor_menu(file->name);		// nove vykresleni
		Print_all(file, &cursor);
		
		//fflush(stdout);
	}
	Call_termios(1);
	printf("\033[?25h");	// show cursor
	
	// ulozeni
	printf("\nSave file? (yes/no): ");
	n = 0;
	My_getline(&response, &n, stdin);
	if (Yes_no(response) == true) {
		printf("\r");
		Save(file);
	}
	
	clear(&file->list);
	//printf("\n");
	return OK;
}

int Save (file_t * file){
	int valid = false;
	size_t n = 0;
	char *name = NULL;
	
	Call_termios(1);
	printf("\033[?25h");	// show cursor
	
	printf("File name? : ");	// (.txt)
	FILE *fp = NULL;
	valid = My_getline(&name, &n, stdin);
	if ( valid == NOK ) {
		printf("ERROR: invalid name: %s !\n", name);
		sleep(1);
		return NOK;
	}
	//Str_merge(&name, name, ".txt");
	fp = fopen(name, "w");
	if ( fp == NULL ) {
		printf("ERROR: canot open file: %s !\n", name);
		sleep(1);
	}
	for (int i = 0; i < size(&file->list); i++) {
		fprintf(fp, "%c", getEntry(&file->list, i));		// pomaly!
	}
	//fprintf (fp, "\n");	 
	fclose (fp);
	printf("\rFile named: %s saved!\n", name);
	if ( 1 || DEBUG ) { 
		sleep(1);
	}
	
	return OK;
}

int Open_file (file_t * file) {
	int ret = OK;
	char c;
	FILE *open_file;
	
	if ( (open_file = fopen (file->name, "r")) == NULL){
		printf ("Error: cannot by opened!\n");
		return NOK;
	} else {
		while ( (c = fgetc(open_file)) != EOF ) {
			if (0 && DEBUG) {
				sleep(1);
				printf("Znak: %c", c);
			}
			Push(&file->list, c);
		}
	} 
	fclose(open_file);
	return ret;
}

void Print_menu (int row, int col) {
	printf("\033[1J");		// clear
	printf("\033[H");		// go home!
	printf("┌─────────────┬──────────────────┬──────────┬─────────┬───────────────────────┐\n");
	printf("│ q - to quit │ h - help & about │ o - open │ n - new │ r - resize : %3d x%3d │\n", row, col);
	printf("└─────────────┴──────────────────┴──────────┴─────────┴───────────────────────┘\n");
	printf("\033[?25h");	// show cursor
}

void Print_editor_menu (char *name) {
	printf("\033[?25l");	// hide cursor
	printf("\033[1J");		// clear
	printf("\033[H");		// go home!
	printf("│ ctrl+q (quit) │ ctrl+s (save) │ name: %s │\n", name);
	printf("└───────────────┴───────────────┴");
	for (int i = 0; i < strlen(name)+8; i++) {
		printf("─");
	}
	printf("┘\n");
	//printf("%lu\n", strlen(name));
}

void Print_about (void) {
	// About editor ◄▼▲► ↔↕↨
	printf(" * Papyrus v0.2.6\n");
	printf(" - Basic text editor. Made by Pytel\n");
	printf(" ■ Editor functions: \n");
	printf("   ► Displays the name of file.\n");
	printf("   ► Backspace implementation.\n");
	printf("   ► Link list integration.\n");
	printf("   ► Ctrl+s to save text file.\n");
	printf("   ► Ctrl+q to quit.\n");
	printf("   ► Open text file.\n");
	printf("   ► Cursor.\n");
	printf("   ► Insert.\n");
	printf("   ► Resizing of window.\n");
	printf("   ? Fast printing.\n");
	printf("   ? handling of letters ěščřž.\n");
	printf("   ? new system of scrolling + scrollbar.\n");
	printf("\n ---Press Enter to continue!---");
	getchar();
}
/*
Modifikace do dalsich verzi:
 - Scroll
 |- Mohl bych přidat vyobrazeni scrollu. 
 |- Propocitat kolik znaku se vejde na obrazovku a zobrazovat jen tolik. 
 	▲
 	|
 	█
 	|
 	|
 	▼
 |- Vykreslovat obrazovku jen kolem kursoru, tak aby byl vzdy pri svem pohybu videt.
 |- generovat vlastni zalomeni - lepsi propocet rozmeru taxtu na monitoru -> lepsi vykrelseni zadaneho textu
 
 - Opravit sipkovani
 |- prestat se na konci propadat na zacatek
 |- preskokovani radku
 |- spatne vykreslovani diakritiky
 |- rychle skoky s page-up/down
 
 - Nastavitelna sirka vykreslovani tabulatoru
 
 - Barevny rezim
 |- slovnik rozpoznavanych slov, definovany v config.txt
  |- vyber barvy
  |- seznam slov, ktera se takto vykresli
 |- vyhodne pouziti v programatorskem modu
 
 - Vice oken
 |- prepinani s tabulatorem mezi nimi
 
 - chyba v ukladani

Hotove:
 - volani s argumentem
 |- automaticky dany soubor otevre, pokud ho najde
*/


/* DEBUG
	FILE *fptr;
	char *file_name = name;
	printf("File: %s\n", file_name);
	fptr = fopen(file_name, "r");
	char znak;
	znak = getc(fptr);
	printf("Znak: %c", znak);
	getchar();
	
	
	//printf("%c", 10);
	//fflush(stdout);
	
	//valid = fread(data, sizeof(unsigned char), Y * 3*X, fd);
	
	char *text = NULL;
	size_t n = 0;
	My_getline(&text, &n, file);
	printf("Text: %s\n", text);
	getchar();
	
	// realokace pameti
	if (aloc == number) {
		aloc = aloc + SIZE;
		text = (char*) realloc (text, (aloc)*sizeof(char));
		if ( text == NULL){
			fprintf(stderr, "Error: Nepodarilo se alokovat misto!\n");
			return NOK;
		}
	}
	//printf("\r%s", text);
	
	for (int i = 0; i < size(); i++) {
		printf("%c", getEntry(i));
	}
	
	if (DEBUG) {
		printf("\nValid: %s", valid ? "true": "false");
		getchar();
	}
	
	printf("\033[999C");		// move fowvard
	printf("\033[999B");		// move down
	
	printf("│ ctrl+q (quit) │ ctrl+s (save) │ name: %10s │\n", name);
	printf("└───────────────┴───────────────┴──────────────────┘\n");
	
	printf("┌─────────────┬──────────────────┬──────────┬─────────┐\n");
	printf("│ q - to quit │ h - help & about │ o - open │ n - new │\n");
	printf("└─────────────┴──────────────────┴──────────┴─────────┘\n");
*/
