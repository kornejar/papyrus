//By Pytel
/*
 * Toto je jednoduhcy testovy editor Papyrus.
 * Umi otevirat textove soubory a taky je ukladat.
 */
//-----Knihovny---------------------------------------------------------------------
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h> 
#include <unistd.h>
//
#include "lib.h"
#include "tools.h"
#include "linked_list.h"
//
#define true 1
#define false 0
#define OK 0
#define NOK -1

#define DEBUG 0
#define SIZE 100

//-----Hlavni_kod-----------------------------------------------------
int main(int argc, char *argv[]){
	// Promene
	// 0D
	size_t n = 0;
	
	// 1D
	char *response = NULL;
	
	file_t file;
	file_init(&file);
	
	// Inicializating the terminal
	// jak vypocitat rozmery terminalu?
	int row, col;
	printf(" Loading! ");
	Get_terminal_size (&row, &col);
	
	// fast open
	if ( argc == 2 ) {
		file_change_name (&file, argv[1]);
		Open_file(&file);
		Editor(&file);
	}
	
	// menu
	int run = true;
	while (run == true) {
		Print_menu(row, col);
		printf("Press key: ");
		My_getline(&response, &n, stdin);
		if (strlen(response) != 1) {
			printf("\rWrong input!\n");
		} else {
			switch (response[0]) {
				case 'h':
					Print_about ();
					break;
				case 'n':
					Editor (&file);
					break;
				case 'o':
					printf("File name: ");
					free(file.name);
					My_getline(&file.name, &n, stdin);
					Open_file(&file);
					Editor(&file);
					break;
				case 'r':
					Get_terminal_size (&row, &col);
					break;
				case 'q':
					run = false;
					break;
				default:
					printf("\rWrong input!\n");
					break;
			}
		}
	}
	
	//printf("\033[2J");	// <ESC> = \033 = 27 - clear
	
	printf("\033[1J");		// clear
	printf("\033[H");		// go home!
	return OK;
}

