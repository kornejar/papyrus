//By Pytel
/*
 * Cursor & print
 */

#include "cursor.h"
#include "linked_list.h"

#define DEBUG 0

//-----Podprogramy------------------------------------------------------------------

void Print_all(file_t * file, int *cursor) {
	int idx = *cursor;
	entry_t *list = file->list.head;
	// pohyblivy kursor
	int i = 0;
	int not_char_len = 0;
	char c;
	while ( i < size(&file->list) ) {
		c = list->znak;
		if ( 0 <= c ) {
			not_char_len = 0;
		} else {
			not_char_len++;
		}
		
		if ( i == idx) {
			if ( i+1 < size(&file->list) && list->znak == 10 ) {	// enter
				printf("▌%c", c);
			} else {									// ostatni znaky
				if ( 0 <= c ) {		// && c <= 255
					printf("\033[4m%c\033[0m", c);
				} else {
					char s[4];
					list = list->prev;
					s[0] = list->znak;
					list = list->next;
					s[1] = list->znak;
					list = list->next;
					s[2] = list->znak;
					s[3] = '\0';
					if (not_char_len%2) {
						printf("\033[4m%c%c\033[0m", s[1], s[2]);
						i++;
					} else {
						printf("%c%c", s[0], s[1]);
						list = list->prev;
						*cursor = ++idx;
					}
				}
			}
		} else {
			printf("%c", c);
		}
		
		// aktualizace
		list = list->next;
		i++;
	}
	if ( idx == size(&file->list) ) {
		printf("▌");
	}
}

/* old
void Print_all(file_t * file, int idx) {
	entry_t *list = file->list.head;
	// pohyblivy kursor
	int i = 0;
	int not_char_len = 0;
	char c;
	while ( i < size(&file->list) ) {
		c = list->znak;
		if ( 0 <= c ) {
			not_char_len = 0;
		} else {
			not_char_len++;
		}
		
		if ( i == idx) {
			if ( i+1 < size(&file->list) && list->znak == 10 ) {	// enter
				printf("▌%c", c);
			} else {									// ostatni znaky
				if ( 0 <= c ) {		// && c <= 255
					printf("\033[4m%c\033[0m", c);
				} else {
					char s[4];
					list = list->prev;
					s[0] = list->znak;
					list = list->next;
					s[1] = list->znak;
					list = list->next;
					s[2] = list->znak;
					s[3] = '\0';
					if ( 0 < s[2] ) {
						printf("\033[4m%c%c\033[0m", s[0], s[1]);
						list = list->prev;
					} else if ( 0 < s[0] ) {
						printf("\033[4m%c%c\033[0m", s[1], s[2]);
					}
					i++;
				}
			}
		} else {
			if (0 <= c ) {
				printf("%c", c);
			} else if ( i != idx-1 && i != idx+1 ) {
				printf("%c", c);
			} else if (not_char_len%2) {
				printf("%c", c);
			}
		}
		
		// aktualizace
		list = list->next;
		i++;
	}
	if ( idx == size(&file->list) ) {
		printf("▌");
	}
}

void Print_all(file_t * file, int idx) {
	entry_t *list = file->list.head;
	// pohyblivy kursor
	for (int i = 0; i < size(&file->list); i++) {
		if ( i == idx) {
			if ( i+1 < size(&file->list) && list->znak == 10 ) {	// enter
				printf("▌%c", list->znak);
			} else {									// ostatni znaky
				printf("\033[4m%c\033[0m", list->znak);
			}
		} else {
			printf("%c", list->znak);
		}
		list = list->next;
	}
	if ( idx == size(&file->list) ) {
		printf("▌");
	}
}

// older

void Print_all(int idx) {
	entry_t *list = header.head;
	entry_t *next_list = NULL;
	// pohyblivy kursor
	for (int i = 0; i < size(); i++) {
		next_list = list->next;
		if ( i+1 == idx && next_list->znak == 10) {	// enter
			printf("▌");
		}
		if ( i == idx) {								// ostatni znaky
			printf("\033[4m%c\033[0m", list->znak);
		} else {
			printf("%c", list->znak);
		}
		list = list->next;
	}
	if ( idx == size() ) {
		printf("▌");
		//printf("\033[4m \033[0m");	// underscore + reset atributes
	}
	
}
// blikajici kursor
// │ ¦ ▌
	for (int i = 0; i < size(); i++) {
		if ( i == idx) {
			printf("\033[5;4m%c\033[0m", list->znak);
		} else {
			printf("%c", list->znak);
		}
		list = list->next;
	}
	if ( idx == size() ) {
		printf("\033[5;4m \033[0m");	// blinking underscore + reset atributes
	}
*/

int Move_up_or_down (file_t *file, int up_or_down, int idx) {
	int row_index = 0;
	int new_index = idx;
	int prev_len = 0;
	int enter = 0;
	entry_t *this_list = file->list.head;
	
	// hledani pozice (indexu na radku)
	for (int i = 0; i < idx; i++) {
		if (this_list->next == NULL) {
			return false;
		}
		if (this_list->znak == 10){			// new line
			row_index = 0;
		}
		row_index++;
		this_list = this_list->next;
	}
	
	// posun na novy index
	if (up_or_down == true) {				// true = up
		while (enter != 2) {
			this_list = this_list->prev;
			if (new_index == 0) {
				break;
			} else if (this_list->znak == 10) {
				enter++;
			} else if ( enter == 1) {
				prev_len++;
			}
			new_index--;
		}
	} else {								// false = down
		while (enter != 2) {
			if ( new_index == size(&file->list) ) {
				return size(&file->list);				// dojel na konec
			} else if (this_list->znak == 10) {
				enter++;
			} else if ( enter == 1) {
				prev_len++;
			}
			if ( enter == 1 && prev_len == row_index) {
				return new_index+1;			// radek je dostatecne dlouhy
			}
			this_list = this_list->next;
			new_index++;
		}
		return new_index+1;					// nasel enter
	}
	
	// return UP
	if (prev_len >= row_index){
		return new_index+row_index;
	} else {
		return prev_len;
	}
}


